'use strict';

exports.registrationDeregisterPOST = function(args, res, next) {
  /**
   * Removes the app from the pipeline server
   * Removes the app from the pipeline server, i.e., for if the app is being removed.
   *
   * uniqueInstanceID String The unique instance ID of this app, provided by the pipeline when registering the app
   * no response value expected for this operation
   **/
  res.end();
}

exports.registrationRegisterGET = function(args, res, next) {
  /**
   * Registers the given app to the server
   * Registers the given app to the server. The registrant must provide a unique name for their app. For example a chat app might have a unique name of org.mitre.tampa.chat 
   *
   * uniqueAppName String This is the unique name for this app
   * friendlyName String The human readable version of this app's unique name
   * version String The version number of this app
   * returns List
   **/
  var examples = {};
  examples['application/json'] = [ {
  "uniqueInstanceID" : "aeiou"
} ];
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

