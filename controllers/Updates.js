'use strict';

var url = require('url');

var Updates = require('./UpdatesService');

module.exports.updateNewPOST = function updateNewPOST (req, res, next) {
  Updates.updateNewPOST(req, req.swagger.params, res, next);
};

module.exports.updateRetrieveAllGET = function updateRetrieveAllGET (req, res, next) {
  Updates.updateRetrieveAllGET(req.swagger.params, res, next);
};

module.exports.updateRetrieveDatetimeGET = function updateRetrieveDatetimeGET (req, res, next) {
  Updates.updateRetrieveDatetimeGET(req.swagger.params, res, next);
};

module.exports.updateSubscribePOST = function updateSubscribePOST (req, res, next) {
  Updates.updateSubscribePOST(req.swagger.params, res, next);
};
