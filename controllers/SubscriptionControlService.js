'use strict';

exports.updateSubscribePOST = function(args, res, next) {
  /**
   * Subscribes the given app instance to an instance of the given publisher app.
   * Subscribes the given app instance to an instance of the given publisher app. Automatically picks a an instance of the publisher app and returns its appInstanceID.
   *
   * subscriberUniqueInstanceID String The Unique ID of the app instance making this request
   * uniqueAppID String The Unique ID of the type of app the requester wants to receive updates from. The pipeline will automatically find an appropriate publisher to meet the needs of the subscriber
   * returns Subscribed
   **/
  var examples = {};
  examples['application/json'] = {
  "uniqueInstanceID" : "aeiou"
};
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

