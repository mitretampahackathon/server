'use strict';

exports.updateRetrieveAllGET = function(args, res, next) {
  /**
   * Retrieves all published data
   * This route retrieves all updates from the publishing apps the requestor is subscribed to.
   *
   * requesterUniqueInstanceID String The Unique Instance ID of the app making this request. This is used to determine what publishing apps the requestor app is subscribed to.
   * returns DataUpdates
   **/
  var examples = {};
  examples['application/json'] = {
  "updatedData" : [ {
    "dateTime" : "aeiou",
    "data" : "aeiou",
    "uniquePublisherID" : "aeiou"
  } ]
};
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.updateRetrieveDatetimeGET = function(args, res, next) {
  /**
   * Temporal-based data retrieval
   * Given a date and time, this route retrieves all updates from the publishing apps the requestor is subscribed to after to the given the date and time. Generally, the data and time provided by the client will be the time it last performed a retrieve operation
   *
   * requesterUniqueInstanceID String The Unique Instance ID of the app making this request. This is used to determine what publishing apps the requestor app is subscribed to.
   * dateTime String The cutoff date and time for this request. The pipeline will return updates from all publishers the requestor is subscribed to that occurred after this date and time.
   * returns DataUpdates
   **/
  var examples = {};
  examples['application/json'] = {
  "updatedData" : [ {
    "dateTime" : "aeiou",
    "data" : "aeiou",
    "uniquePublisherID" : "aeiou"
  } ]
};
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

