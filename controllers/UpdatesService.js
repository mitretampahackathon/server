'use strict';

exports.updateNewPOST = function(req, args, res, next) {
	var db = req.app.get('db');
	console.log(args[0]);
  /**
   * Publishers use this route to push new data to subscribers
   * Publishers use this route to push new data to subscribers. New data is provided by the publisher as a JSON formatted string
   *
   * publisherUniqueInstanceID String The unique ID of this publisher, used by the pipeline to determine what subscribers will receive this update
   * newData String The new data being published
   * no response value expected for this operation
   **/
   
  res.end();
}

exports.updateRetrieveAllGET = function(args, res, next) {
  /**
   * Retrieves all published data
   * This route retrieves all updates from the publishing apps the requestor is subscribed to.
   *
   * requesterUniqueInstanceID String The Unique Instance ID of the app making this request. This is used to determine what publishing apps the requestor app is subscribed to.
   * returns DataUpdates
   **/
  var examples = {};
  examples['application/json'] = {
  "updatedData" : [ {
    "dateTime" : "aeiou",
    "data" : "aeiou",
    "uniquePublisherID" : "aeiou"
  } ]
};
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.updateRetrieveDatetimeGET = function(args, res, next) {
  /**
   * Temporal-based data retrieval
   * Given a date and time, this route retrieves all updates from the publishing apps the requestor is subscribed to after to the given the date and time. Generally, the data and time provided by the client will be the time it last performed a retrieve operation
   *
   * requesterUniqueInstanceID String The Unique Instance ID of the app making this request. This is used to determine what publishing apps the requestor app is subscribed to.
   * dateTime String The cutoff date and time for this request. The pipeline will return updates from all publishers the requestor is subscribed to that occurred after this date and time.
   * returns DataUpdates
   **/
  var examples = {};
  examples['application/json'] = {
  "updatedData" : [ {
    "dateTime" : "aeiou",
    "data" : "aeiou",
    "uniquePublisherID" : "aeiou"
  } ]
};
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.updateSubscribePOST = function(args, res, next) {
  /**
   * Subscribes the given app instance to an instance of the given publisher app.
   * Subscribes the given app instance to an instance of the given publisher app. Automatically picks a an instance of the publisher app and returns its appInstanceID.
   *
   * subscriberUniqueInstanceID String The Unique ID of the app instance making this request
   * uniqueAppID String The Unique ID of the type of app the requester wants to receive updates from. The pipeline will automatically find an appropriate publisher to meet the needs of the subscriber
   * returns Subscribed
   **/
  var examples = {};
  examples['application/json'] = {
  "uniqueInstanceID" : "aeiou"
};
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

