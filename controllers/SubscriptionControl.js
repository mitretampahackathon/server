'use strict';

var url = require('url');

var SubscriptionControl = require('./SubscriptionControlService');

module.exports.updateSubscribePOST = function updateSubscribePOST (req, res, next) {
  SubscriptionControl.updateSubscribePOST(req.swagger.params, res, next);
};
