'use strict';

var url = require('url');

var Retrieval = require('./RetrievalService');

module.exports.updateRetrieveAllGET = function updateRetrieveAllGET (req, res, next) {
  Retrieval.updateRetrieveAllGET(req.swagger.params, res, next);
};

module.exports.updateRetrieveDatetimeGET = function updateRetrieveDatetimeGET (req, res, next) {
  Retrieval.updateRetrieveDatetimeGET(req.swagger.params, res, next);
};
