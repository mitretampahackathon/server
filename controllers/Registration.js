'use strict';

var url = require('url');

var Registration = require('./RegistrationService');

module.exports.registrationDeregisterPOST = function registrationDeregisterPOST (req, res, next) {
  Registration.registrationDeregisterPOST(req.swagger.params, res, next);
};

module.exports.registrationRegisterGET = function registrationRegisterGET (req, res, next) {
  Registration.registrationRegisterGET(req.swagger.params, res, next);
};
