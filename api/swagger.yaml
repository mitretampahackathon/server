---
swagger: "2.0"
info:
  description: "Move your app forward with the Uber API"
  version: "1.0.0"
  title: "Uber API"
host: "api.uber.com"
basePath: "/v1"
schemes:
- "https"
produces:
- "application/json"
paths:
  /registration/register:
    get:
      tags:
      - "Registration"
      summary: "Registers the given app to the server"
      description: "Registers the given app to the server. The registrant must provide\
        \ a unique name for their app.\nFor example a chat app might have a unique\
        \ name of org.mitre.tampa.chat\n"
      operationId: "registrationRegisterGET"
      parameters:
      - name: "uniqueAppName"
        in: "query"
        description: "This is the unique name for this app"
        required: true
        type: "string"
      - name: "friendlyName"
        in: "query"
        description: "The human readable version of this app's unique name"
        required: true
        type: "string"
      - name: "version"
        in: "query"
        description: "The version number of this app"
        required: true
        type: "string"
      responses:
        200:
          description: "Confirmation of registration. Also returns a unique ID representing\
            \ this particular instance of the app"
          schema:
            type: "array"
            items:
              $ref: "#/definitions/Registered"
        default:
          description: "Unexpected error"
          schema:
            $ref: "#/definitions/Error"
      x-swagger-router-controller: "Registration"
  /registration/deregister:
    post:
      tags:
      - "Registration"
      summary: "Removes the app from the pipeline server"
      description: "Removes the app from the pipeline server, i.e., for if the app\
        \ is being removed."
      operationId: "registrationDeregisterPOST"
      parameters:
      - name: "uniqueInstanceID"
        in: "formData"
        description: "The unique instance ID of this app, provided by the pipeline\
          \ when registering the app"
        required: true
        type: "string"
      responses:
        200:
          description: "Confirms that deregistration was successful"
        404:
          description: "The given uniqueInstanceID is not registered to the pipeline\
            \ server"
      x-swagger-router-controller: "Registration"
  /update/retrieve/datetime:
    get:
      tags:
      - "Retrieval"
      - "Updates"
      summary: "Temporal-based data retrieval"
      description: "Given a date and time, this route retrieves all updates from the\
        \ publishing apps the requestor is subscribed to after to the given the date\
        \ and time. Generally, the data and time provided by the client will be the\
        \ time it last performed a retrieve operation"
      operationId: "updateRetrieveDatetimeGET"
      parameters:
      - name: "requesterUniqueInstanceID"
        in: "query"
        description: "The Unique Instance ID of the app making this request. This\
          \ is used to determine what publishing apps the requestor app is subscribed\
          \ to."
        required: true
        type: "string"
      - name: "dateTime"
        in: "query"
        description: "The cutoff date and time for this request. The pipeline will\
          \ return updates from all publishers the requestor is subscribed to that\
          \ occurred after this date and time."
        required: true
        type: "string"
        format: "dateTime"
      responses:
        200:
          description: "Successful request. This will return an array of the updates\
            \ from the requestor's publishers. Simply returns an empty array if the\
            \ requestor is not subscribed to any publishers, or no data is available"
          schema:
            $ref: "#/definitions/DataUpdates"
        404:
          description: "The given uniqueInstanceID is not registered to the pipeline\
            \ server"
      x-swagger-router-controller: "Retrieval"
  /update/retrieve/all:
    get:
      tags:
      - "Retrieval"
      - "Updates"
      summary: "Retrieves all published data"
      description: "This route retrieves all updates from the publishing apps the\
        \ requestor is subscribed to."
      operationId: "updateRetrieveAllGET"
      parameters:
      - name: "requesterUniqueInstanceID"
        in: "query"
        description: "The Unique Instance ID of the app making this request. This\
          \ is used to determine what publishing apps the requestor app is subscribed\
          \ to."
        required: true
        type: "string"
      responses:
        200:
          description: "Successful request. This will return an array of the updates\
            \ from the requestor's publishers. Simply returns an empty array if the\
            \ requestor is not subscribed to any publishers, or no data is available"
          schema:
            $ref: "#/definitions/DataUpdates"
        404:
          description: "The given uniqueInstanceID is not registered to the pipeline\
            \ server"
      x-swagger-router-controller: "Retrieval"
  /update/subscribe:
    post:
      tags:
      - "Updates"
      - "Subscription Control"
      summary: "Subscribes the given app instance to an instance of the given publisher\
        \ app."
      description: "Subscribes the given app instance to an instance of the given\
        \ publisher app. Automatically picks a an instance of the publisher app and\
        \ returns its appInstanceID."
      operationId: "updateSubscribePOST"
      parameters:
      - name: "subscriberUniqueInstanceID"
        in: "formData"
        description: "The Unique ID of the app instance making this request"
        required: true
        type: "string"
      - name: "uniqueAppID"
        in: "formData"
        description: "The Unique ID of the type of app the requester wants to receive\
          \ updates from. The pipeline will automatically find an appropriate publisher\
          \ to meet the needs of the subscriber"
        required: true
        type: "string"
      responses:
        200:
          description: "Subscription successfully added. This will return the unique\
            \ instance ID of the selected publisher"
          schema:
            $ref: "#/definitions/Subscribed"
        404:
          description: "No instances of the given app type are registered to the pipeline\
            \ server"
      x-swagger-router-controller: "Updates"
  /update/new:
    post:
      tags:
      - "Updates"
      summary: "Publishers use this route to push new data to subscribers"
      description: "Publishers use this route to push new data to subscribers. New\
        \ data is provided by the publisher as a JSON formatted string"
      operationId: "updateNewPOST"
      parameters:
      - name: "publisherUniqueInstanceID"
        in: "formData"
        description: "The unique ID of this publisher, used by the pipeline to determine\
          \ what subscribers will receive this update"
        required: true
        type: "string"
      - name: "newData"
        in: "formData"
        description: "The new data being published"
        required: true
        type: "string"
      responses:
        200:
          description: "Data posted successfully"
        404:
          description: "The given unique publisher instance ID does not exist on the\
            \ server"
      x-swagger-router-controller: "Updates"
definitions:
  Registered:
    type: "object"
    properties:
      uniqueInstanceID:
        type: "string"
        description: "Unique identifier representing this particular instance of the\
          \ app."
  Subscribed:
    type: "object"
    properties:
      uniqueInstanceID:
        type: "string"
        description: "The Unique Identifier returned to the subscriber when it subscribes\
          \ to a publisher"
  DataUpdates:
    type: "object"
    properties:
      updatedData:
        type: "array"
        items:
          $ref: "#/definitions/DataUpdate"
  DataUpdate:
    type: "object"
    properties:
      uniquePublisherID:
        type: "string"
        description: "The unique instance ID of this update's publisher"
      dateTime:
        type: "string"
        format: "dateTime"
        description: "The date and time this update was posted"
      data:
        type: "string"
        description: "The new data, sent as a JSON-formatted string"
  Error:
    type: "object"
    properties:
      code:
        type: "integer"
        format: "int32"
      message:
        type: "string"
      fields:
        type: "string"
